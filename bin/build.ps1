$ErrorActionPreference = 'Stop'

#Project ID
$id = 9731
# Paths to files
$rootPath = "$PSScriptRoot\.."
$binPath = "$rootPath\bin"
$buildPath = "$rootPath\build"
$codePath = "$rootPath\code"
$sourcePath = "$rootPath\source"

# Load the content of the CSV file
$csvContent = Import-Csv -Path "$sourcePath\w3strings\languages.csv" -Encoding Default
$languages = @{}
$index = 0
# Loop through each line of the CSV
foreach ($row in $csvContent) {
    # Loop through each column (except for the 'key' column).
    foreach ($col in $row.PSObject.Properties | Where-Object { $_.Name -ne "key" }) {
        # Check if the language exists in the associative array, otherwise create it.
        if (-not $languages.ContainsKey($col.Name)) {
            $languages[$col.Name] = @()
        }
        # Add the line to the associative array.
        $languages[$col.Name] += "2119731$($index.ToString("D3"))| |$($row.key)|$($col.Value)"
    }
    $index++
}
# Loop through each language and create a file with the formatted content.
foreach ($lang in $languages.Keys) {
    $outputFileName = "$lang"
    $languageData = ";meta[language=$lang]",
    "; id |key(hex)|key(str)| text",
    ";",
    "; Comment lines start with ;",
    "; All comment lines (except the first two header lines) will be ignored",
    "; Enhanced Targeting's Nexus ID is 1432, so there are 211-1432-xxxx left",
    ";",
    "; ITOW's Nexus ID is 9731",
    ";"
    $languageData += $languages[$lang]
    # Write the content to the output file.
    $languageData | Set-Content -Path "$sourcePath\w3strings\languages\$outputFileName" -Encoding Default -Force
}
# Encode w3strings files
& "$binPath\w3strings.exe" --encode "$sourcePath\w3strings\languages\en" --id-space $id
& "$binPath\w3strings.exe" --encode "$sourcePath\w3strings\languages\fr" --id-space $id
# Move encoded files with the -Force parameter
Move-Item -Path "$sourcePath\w3strings\languages\en.w3strings" -Destination "$codePath\mods\modInTimesOfWar\content\en.w3strings" -Force
Move-Item -Path "$sourcePath\w3strings\languages\fr.w3strings" -Destination "$codePath\mods\modInTimesOfWar\content\fr.w3strings" -Force
# Remove temporary files
Remove-Item "$sourcePath\w3strings\languages\*.w3strings.ws"
# Create compressed archive
$archiveName = "InTimesOfWar.zip"
Compress-Archive -Path "$codePath\*" -DestinationPath "$buildPath\$archiveName" -Force

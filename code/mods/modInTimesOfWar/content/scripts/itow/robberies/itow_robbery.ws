class ITOW_Robbery extends ITOW_Entity
{
    private const var CLASS_NAME        : CName;                default CLASS_NAME  = 'ITOW_Robbery';
    private var inventory               : CInventoryComponent;
    private var ITOW_config             : ITOW_Config;
    private var ITOW_displayer          : ITOW_Displayer;
    private var ITOW_item               : ITOW_Item;
    private var itemIDs                 : array<SItemUniqueId>;
    private var lastMoneyAmountStolen   : int;
    private var lastItemsStolen         : array<CName>;

    event OnDestroy(): void
    {
        DisplayNotification("Destroyed.");
    }

    event OnInit(): void
    {
        ITOW_config = new ITOW_Config in this;
        ITOW_displayer = new ITOW_Displayer in this;
        ITOW_item = new ITOW_Item in this;
        ITOW_config.OnInit();
        ITOW_displayer.OnInit();
        ITOW_item.OnInit();
        DisplayNotification("Initialized.");
    }

    public function Start(): void
    {
        if (!GetConfigEnabled()) return;
        lastItemsStolen.Clear();
        lastMoneyAmountStolen = 0;
        TryToRobMoney();
        TryToRobItems();
        DisplayNotification("Money Stolen: " + lastMoneyAmountStolen + ". Items stolen: " + LastItemsStolenToString() + ".");
    }

    private function DisplayMessage(message: string): void
    {
        ITOW_displayer.DisplayMessage(message);
    }

    private function DisplayNotification(notification: string): void
    {
        if (!GetConfigDebugMode()) return;
        ITOW_displayer.DisplayNotification(GetClassName() + ": " + notification);
    }

    private function GetClassName(): CName
    {
        return CLASS_NAME;
    }

    private function GetChanceOfGettingItemStolen(itemID: SItemUniqueId): int
    {
        if (inventory.IsItemQuest(itemID))                  return GetConfigChanceOfGettingQuestItemStolen();
        if (inventory.IsRecipeOrSchematic(itemID))          return GetConfigChanceOfGettingRecipeOrSchematicStolen();
        if (inventory.IsItemAlchemyItem(itemID))            return GetConfigChanceOfGettingAlchemyItemStolen();
        if (inventory.IsItemAnyArmor(itemID))               return GetConfigChanceOfGettingArmorStolen();
        if (inventory.IsItemBolt(itemID))                   return GetConfigChanceOfGettingBoltStolen();
        if (inventory.IsItemDye(itemID))                    return GetConfigChanceOfGettingDyeStolen();
        if (inventory.IsItemFood(itemID))                   return GetConfigChanceOfGettingFoodStolen();
        if (inventory.IsItemHorseItem(itemID))              return GetConfigChanceOfGettingHorseItemStolen();
        if (inventory.IsItemIngredient(itemID))             return GetConfigChanceOfGettingIngredientStolen();
        if (inventory.IsItemJunk(itemID))                   return GetConfigChanceOfGettingJunkStolen();
        if (inventory.IsItemMask(itemID))                   return GetConfigChanceOfGettingMaskStolen();
        if (inventory.IsItemReadable(itemID))               return GetConfigChanceOfGettingReadableStolen();
        if (inventory.IsItemTrophy(itemID))                 return GetConfigChanceOfGettingTrophyStolen();
        if (inventory.IsItemUpgrade(itemID))                return GetConfigChanceOfGettingUpgradeItemStolen();
        if (inventory.IsItemWeapon(itemID))                 return GetConfigChanceOfGettingWeaponStolen();
        return 0;
    }

    private function GetConfigChanceOfGettingAlchemyItemStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingAlchemyItemStolen'));
    }

    private function GetConfigChanceOfGettingArmorStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingArmorStolen'));
    }

    private function GetConfigChanceOfGettingBoltStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingBoltStolen'));
    }

    private function GetConfigChanceOfGettingDyeStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingDyeStolen'));
    }

    private function GetConfigChanceOfGettingFoodStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingFoodStolen'));
    }

    private function GetConfigChanceOfGettingHorseItemStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingHorseItemStolen'));
    }

    private function GetConfigChanceOfGettingIngredientStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingIngredientStolen'));
    }

    private function GetConfigChanceOfGettingJunkStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingJunkStolen'));
    }

    private function GetConfigChanceOfGettingMaskStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingMaskStolen'));
    }

    private function GetConfigChanceOfGettingQuestItemStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingQuestItemStolen'));
    }

    private function GetConfigChanceOfGettingReadableStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingReadableStolen'));
    }

    private function GetConfigChanceOfGettingRecipeOrSchematicStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingRecipeOrSchematicStolen'));
    }

    private function GetConfigChanceOfGettingTrophyStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingTrophyStolen'));
    }

    private function GetConfigChanceOfGettingUpgradeItemStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingUpgradeItemStolen'));
    }

    private function GetConfigChanceOfGettingWeaponStolen(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingWeaponStolen'));
    }

    private function GetConfigChanceMultiplierPerItemQuality(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceMultiplierPerItemQuality'));
    }

    private function GetConfigDebugMode(): bool
    {
        return ITOW_config.Get(GetClassName(), 'ITOW_RobberyDebugEnabled');
    }

    private function GetConfigEnabled(): bool
    {
        return ITOW_config.Get(GetClassName(), 'ITOW_RobberyEnabled');
    }

    private function GetConfigMoneyStolenAmountMaxPercent(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyMoneyStolenAmountMaxPercent'));
    }

    private function HowManyItemsRobbed(itemID: SItemUniqueId): int
    {
        return 999;
    }

    private function HowMuchMoneyRobbed(): int
    {
        var money   : float;
        var amount  : float;
        money = GetWitcherPlayer().GetMoney();
        amount = RandRange((int)(money * (GetConfigMoneyStolenAmountMaxPercent() / 100.f)));
        return (int)amount;
    }

    private function LastItemsStolenToString(): string
    {
        var i   : int;
        var str : string;
        str = "";
        for (i = 0; i < lastItemsStolen.Size(); i += 1)
        {
            str += lastItemsStolen[i] + ",";
        }
        if (str == "") str = "Nothing";
        return str;
    }

    private function ShouldItemBeRobbed(itemID: SItemUniqueId): bool
    {
        var chance              : int;
        var chanceBase          : int;
        var chanceMultiplier    : float;
        chanceBase = GetChanceOfGettingItemStolen(itemID);
        chanceMultiplier = ITOW_item.GetQuality(itemID) * GetConfigChanceMultiplierPerItemQuality();
        chance = (int)(chanceBase * chanceMultiplier);
        return chance >= RandRange(100) + 1;
    }

    private function ShouldMoneyBeRobbed(): bool
    {
        var chance: int;
        chance = StringToInt(ITOW_config.Get(GetClassName(), 'ITOW_RobberyChanceOfGettingMoneyStolen'));
        return chance >= RandRange(100) + 1;
    }

    private function TryToRobItems(): void
    {
        var i       : int;
        var itemID  : SItemUniqueId;
        inventory = GetWitcherPlayer().GetInventory();
        inventory.GetAllItems(itemIDs);
        for (i = 0; i < itemIDs.Size(); i += 1) {
            itemID = itemIDs[i];
            if (ShouldItemBeRobbed(itemID)) {
                lastItemsStolen.PushBack(inventory.GetItemName(itemID));
                inventory.RemoveItem(itemID, HowManyItemsRobbed(itemID));
            }
        }
    }

    private function TryToRobMoney(): void
    {
        if (!ShouldMoneyBeRobbed()) return;
        lastMoneyAmountStolen = HowMuchMoneyRobbed();
        GetWitcherPlayer().RemoveMoney(lastMoneyAmountStolen);
    }
}

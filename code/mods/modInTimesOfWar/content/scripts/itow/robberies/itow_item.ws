class ITOW_Item extends ITOW_Entity
{
    public function GetQuality(itemID: SItemUniqueId): int
    {
        return GetWitcherPlayer().GetInventory().GetItemQuality(itemID);
    }

    public function IsAlcohol(itemID: SItemUniqueId): bool
    {
        return GetWitcherPlayer().GetInventory().ItemHasTag(itemID, 'Alcohol');
    }

    public function IsDrink(itemID: SItemUniqueId): bool
    {
        return GetWitcherPlayer().GetInventory().ItemHasTag(itemID, 'Drinks');
    }

    public function IsFood(itemID: SItemUniqueId): bool
    {
        return GetWitcherPlayer().GetInventory().ItemHasTag(itemID, 'Edibles');
    }

    public function IsPotion(itemID: SItemUniqueId): bool
    {
        return GetWitcherPlayer().GetInventory().IsItemPotion(itemID);
    }
}

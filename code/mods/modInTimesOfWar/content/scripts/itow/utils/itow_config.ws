class ITOW_Config extends ITOW_Entity
{
    private var inGameConfigWrapper : CInGameConfigWrapper;

    event OnInit(): void
    {
        inGameConfigWrapper = theGame.GetInGameConfigWrapper();
    }

    public function Get(group: CName, key: CName): string
    {
        return inGameConfigWrapper.GetVarValue(group, key);
    }

    public function Set(group: CName, key: CName, value: string): void
    {
        inGameConfigWrapper.SetVarValue(group, key, value);
    }
}

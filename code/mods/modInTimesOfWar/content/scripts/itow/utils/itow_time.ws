class ITOW_Time extends ITOW_Entity
{
    public function GetGameHoursPerRealMinutes(minutes: int): float
    {
        return theGame.GetHoursPerMinute() * minutes;
    }

    public function GetGameMinutesInRealSeconds(minutes: int): float
    {
        return (60 / (GetGameHoursPerRealMinutes(minutes) * 60));
    }
}

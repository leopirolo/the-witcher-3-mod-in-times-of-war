class ITOW_Displayer extends ITOW_Entity
{
    public function DisplayMessage(message: string): void
    {
        GetWitcherPlayer().ITOW_DisplayMessage(message);
    }

    public function DisplayNotification(notification: string): void
    {
        GetWitcherPlayer().ITOW_DisplayNotification(notification);
    }
}

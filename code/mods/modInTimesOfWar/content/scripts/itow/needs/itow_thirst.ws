class ITOW_Thirst extends ITOW_Need
{
    default CLASS_NAME                                          = 'ITOW_Thirst';
    default CONFIG_DEATH_ON_DEPLETED_KEY                        = 'ITOW_ThirstDeathWhenDepleted';
    default CONFIG_DEBUG_MODE_KEY                               = 'ITOW_ThirstDebugEnabled';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_RUNNING_KEY        = 'ITOW_ThirstDepletionMultiplierWhenRunning';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_SPRINTING_KEY      = 'ITOW_ThirstDepletionMultiplierWhenSprinting';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_SWIMMING_KEY       = 'ITOW_ThirstDepletionMultiplierWhenSwimming';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_WALKING_KEY        = 'ITOW_ThirstDepletionMultiplierWhenWalking';
    default CONFIG_DEPLETION_PER_ATTACK_KEY                     = 'ITOW_ThirstDepletionPerAttack';
    default CONFIG_DEPLETION_PER_CONSUMED_ALCOHOL_KEY           = 'ITOW_ThirstDepletionPerConsumedAlcohol';
    default CONFIG_DEPLETION_PER_CONSUMED_FOOD_KEY              = 'ITOW_ThirstDepletionPerConsumedFood';
    default CONFIG_DEPLETION_PER_CONSUMED_POTION_KEY            = 'ITOW_ThirstDepletionPerConsumedPotion';
    default CONFIG_DEPLETION_PER_COUNTER_KEY                    = 'ITOW_ThirstDepletionPerCounter';
    default CONFIG_DEPLETION_PER_DODGE_KEY                      = 'ITOW_ThirstDepletionPerDodge';
    default CONFIG_DEPLETION_PER_JUMP_KEY                       = 'ITOW_ThirstDepletionPerJump';
    default CONFIG_DEPLETION_PER_PARRY_KEY                      = 'ITOW_ThirstDepletionPerParry';
    default CONFIG_DEPLETION_PER_ROLL_KEY                       = 'ITOW_ThirstDepletionPerRoll';
    default CONFIG_ENABLED_KEY                                  = 'ITOW_ThirstEnabled';
    default CONFIG_GAME_HOURLY_DEPLETION_KEY                    = 'ITOW_ThirstGameHourlyDepletion';
    default CONFIG_MESSAGE_FREQUENCY_IN_HOUR_KEY                = 'ITOW_ThirstMessageFrequencyInHour';
    default CONFIG_VALUE_KEY                                    = 'ITOW_ThirstValue';
    private const var CONFIG_RESTORATION_PER_CONSUMED_DRINK_KEY : CName;
    default CONFIG_RESTORATION_PER_CONSUMED_DRINK_KEY           = 'ITOW_ThirstRestorationPerConsumedDrink';

    private function GetConfigDepletionPerConsumedDrink(): float
    {
        return -GetConfigRestorationPerConsumedDrink();
    }

    private function GetConfigRestorationPerConsumedDrink(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigRestorationPerConsumedDrinkKey()));
    }

    private function GetConfigRestorationPerConsumedDrinkKey(): CName
    {
        return CONFIG_RESTORATION_PER_CONSUMED_DRINK_KEY;
    }

    private function SetMessagesAlert(): void
    {
        var i: int;
        for (i = 0; i <= 3; i += 1) alertMessages.PushBack(GetLocStringByKeyExt("message_ITOW_ThirstAlertMessage0" + i));
    }

    private function SetMessagesDying(): void
    {
        var i: int;
        for (i = 0; i <= 0; i += 1) dyingMessages.PushBack(GetLocStringByKeyExt("message_ITOW_ThirstDyingMessage0" + i));
    }

    private function SetMessagesSafe(): void
    {
        var i: int;
        for (i = 0; i <= 4; i += 1) safeMessages.PushBack(GetLocStringByKeyExt("message_ITOW_ThirstSafeMessage0" + i));
    }

    private function SetMessagesWarning(): void
    {
        var i: int;
        for (i = 0; i <= 4; i += 1) warningMessages.PushBack(GetLocStringByKeyExt("message_ITOW_ThirstWarningMessage0" + i));
    }
}

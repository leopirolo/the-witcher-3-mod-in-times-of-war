class ITOW_Energy extends ITOW_Need
{
    default CLASS_NAME                                                  = 'ITOW_Energy';
    default CONFIG_DEATH_ON_DEPLETED_KEY                                = 'ITOW_EnergyDeathWhenDepleted';
    default CONFIG_DEBUG_MODE_KEY                                       = 'ITOW_EnergyDebugEnabled';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_RUNNING_KEY                = 'ITOW_EnergyDepletionMultiplierWhenRunning';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_SPRINTING_KEY              = 'ITOW_EnergyDepletionMultiplierWhenSprinting';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_SWIMMING_KEY               = 'ITOW_EnergyDepletionMultiplierWhenSwimming';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_WALKING_KEY                = 'ITOW_EnergyDepletionMultiplierWhenWalking';
    default CONFIG_DEPLETION_PER_ATTACK_KEY                             = 'ITOW_EnergyDepletionPerAttack';
    default CONFIG_DEPLETION_PER_CONSUMED_ALCOHOL_KEY                   = 'ITOW_EnergyDepletionPerConsumedAlcohol';
    default CONFIG_DEPLETION_PER_CONSUMED_DRINK_KEY                     = 'ITOW_EnergyDepletionPerConsumedDrink';
    default CONFIG_DEPLETION_PER_CONSUMED_FOOD_KEY                      = 'ITOW_EnergyDepletionPerConsumedFood';
    default CONFIG_DEPLETION_PER_COUNTER_KEY                            = 'ITOW_EnergyDepletionPerCounter';
    default CONFIG_DEPLETION_PER_DODGE_KEY                              = 'ITOW_EnergyDepletionPerDodge';
    default CONFIG_DEPLETION_PER_JUMP_KEY                               = 'ITOW_EnergyDepletionPerJump';
    default CONFIG_DEPLETION_PER_PARRY_KEY                              = 'ITOW_EnergyDepletionPerParry';
    default CONFIG_DEPLETION_PER_ROLL_KEY                               = 'ITOW_EnergyDepletionPerRoll';
    default CONFIG_ENABLED_KEY                                          = 'ITOW_EnergyEnabled';
    default CONFIG_GAME_HOURLY_DEPLETION_KEY                            = 'ITOW_EnergyGameHourlyDepletion';
    default CONFIG_MESSAGE_FREQUENCY_IN_HOUR_KEY                        = 'ITOW_EnergyMessageFrequencyInHour';
    default CONFIG_VALUE_KEY                                            = 'ITOW_EnergyValue';
    private const var CONFIG_GAME_HOURLY_RESTORATION_BY_MEDITATION_KEY  : CName;
    private const var CONFIG_RESTORATION_PER_CONSUMED_POTION_KEY        : CName;
    default CONFIG_GAME_HOURLY_RESTORATION_BY_MEDITATION_KEY            = 'ITOW_EnergyGameHourlyRestorationByMeditation';
    default CONFIG_RESTORATION_PER_CONSUMED_POTION_KEY                  = 'ITOW_EnergyRestorationPerConsumedPotion';

    event OnMeditationEnd(gameMinutesPassed: float): void
    {
        var gameHoursPassed: float;
        gameHoursPassed = gameMinutesPassed / 60.f;
        UpdateValueBy(GetConfigGameHourlyRestorationByMeditation() * gameHoursPassed, 'OnMeditationEnd');
    }

    private function GetConfigDepletionPerConsumedPotion(): float
    {
        return -GetConfigRestorationPerConsumedPotion();
    }

    private function GetConfigGameHourlyRestorationByMeditation(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), GetConfigGameHourlyRestorationByMeditationKey()));
    }

    private function GetConfigGameHourlyRestorationByMeditationKey(): CName
    {
        return CONFIG_GAME_HOURLY_RESTORATION_BY_MEDITATION_KEY;
    }

    private function GetConfigRestorationPerConsumedPotion(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigRestorationPerConsumedPotionKey()));
    }

    private function GetConfigRestorationPerConsumedPotionKey(): CName
    {
        return CONFIG_RESTORATION_PER_CONSUMED_POTION_KEY;
    }

    private function SetMessagesAlert(): void
    {
        var i: int;
        for (i = 0; i <= 4; i += 1) alertMessages.PushBack(GetLocStringByKeyExt("message_ITOW_EnergyAlertMessage0" + i));
    }

    private function SetMessagesDying(): void
    {
        var i: int;
        for (i = 0; i <= 0; i += 1) dyingMessages.PushBack(GetLocStringByKeyExt("message_ITOW_EnergyDyingMessage0" + i));
    }

    private function SetMessagesSafe(): void
    {
        var i: int;
        for (i = 0; i <= 4; i += 1) safeMessages.PushBack(GetLocStringByKeyExt("message_ITOW_EnergySafeMessage0" + i));
    }

    private function SetMessagesWarning(): void
    {
        var i: int;
        for (i = 0; i <= 4; i += 1) warningMessages.PushBack(GetLocStringByKeyExt("message_ITOW_EnergyWarningMessage0" + i));
    }
}

abstract class ITOW_Need extends ITOW_Entity
{
    protected var ITOW_config                                           : ITOW_Config;
    protected var alertMessages                                         : array<string>;
    protected var dyingMessages                                         : array<string>;
    protected var safeMessages                                          : array<string>;
    protected var warningMessages                                       : array<string>;
    private const var CLASS_NAME                                        : CName;
    private const var CONFIG_DEATH_ON_DEPLETED_KEY                      : CName;
    private const var CONFIG_DEBUG_MODE_KEY                             : CName;
    private const var CONFIG_DEPLETION_MULTIPLIER_WHEN_RUNNING_KEY      : CName;
    private const var CONFIG_DEPLETION_MULTIPLIER_WHEN_SPRINTING_KEY    : CName;
    private const var CONFIG_DEPLETION_MULTIPLIER_WHEN_SWIMMING_KEY     : CName;
    private const var CONFIG_DEPLETION_MULTIPLIER_WHEN_WALKING_KEY      : CName;
    private const var CONFIG_DEPLETION_PER_ATTACK_KEY                   : CName;
    private const var CONFIG_DEPLETION_PER_CONSUMED_ALCOHOL_KEY         : CName;
    private const var CONFIG_DEPLETION_PER_CONSUMED_DRINK_KEY           : CName;
    private const var CONFIG_DEPLETION_PER_CONSUMED_FOOD_KEY            : CName;
    private const var CONFIG_DEPLETION_PER_CONSUMED_POTION_KEY          : CName;
    private const var CONFIG_DEPLETION_PER_COUNTER_KEY                  : CName;
    private const var CONFIG_DEPLETION_PER_DODGE_KEY                    : CName;
    private const var CONFIG_DEPLETION_PER_JUMP_KEY                     : CName;
    private const var CONFIG_DEPLETION_PER_PARRY_KEY                    : CName;
    private const var CONFIG_DEPLETION_PER_ROLL_KEY                     : CName;
    private const var CONFIG_ENABLED_KEY                                : CName;
    private const var CONFIG_GAME_HOURLY_DEPLETION_KEY                  : CName;
    private const var CONFIG_MESSAGE_FREQUENCY_IN_HOUR_KEY              : CName;
    private const var CONFIG_VALUE_KEY                                  : CName;
    private const var ALERT_THRESHOLD                                   : int;              default ALERT_THRESHOLD     = 10;
    private const var MAX                                               : int;              default MAX                 = 100;
    private const var MIN                                               : int;              default MIN                 = 0;
    private const var WARNING_THRESHOLD                                 : int;              default WARNING_THRESHOLD   = 25;
    private var ITOW_displayer                                          : ITOW_Displayer;
    private var ITOW_item                                               : ITOW_Item;
    private var lastMessageGameTime                                     : GameTime;
    private var value                                                   : float;

    event OnAttack(): void
    {
        UpdateValueBy(-GetConfigDepletionPerAttack(), 'OnAttack');
    }

    event OnConsumeEdible(itemID: SItemUniqueId): void
    {
        if (ITOW_item.IsAlcohol(itemID))    UpdateValueBy(-GetConfigDepletionPerConsumedAlcohol(), 'OnConsumeEdible');
        else if (ITOW_item.IsDrink(itemID)) UpdateValueBy(-GetConfigDepletionPerConsumedDrink(), 'OnConsumeEdible');
        else if (ITOW_item.IsFood(itemID))  UpdateValueBy(-GetConfigDepletionPerConsumedFood(), 'OnConsumeEdible');
    }

    event OnConsumePotion(): void
    {
        UpdateValueBy(-GetConfigDepletionPerConsumedPotion(), 'OnConsumePotion');
    }

    event OnDestroy(): void
    {
        DisplayNotification("Destroyed.");
    }

    event OnDodge(): void
    {
        UpdateValueBy(-GetConfigDepletionPerDodge(), 'OnDodge');
    }

    event OnEnteringAlertRange(): void
    {
        DisplayMessage("*" + GetRandomMessage(alertMessages) + "*");
        lastMessageGameTime = theGame.GetGameTime();
    }

    event OnEnteringMax(): void
    {}

    event OnEnteringMin(): void
    {
        if (GetConfigDeathOnDepleted()) {
            DisplayMessage("*" + GetRandomMessage(dyingMessages) + "*");
            lastMessageGameTime = theGame.GetGameTime();
        }
    }

    event OnEnteringSafeRange(): void
    {
        DisplayMessage("*" + GetRandomMessage(safeMessages) + "*");
        lastMessageGameTime = theGame.GetGameTime();
    }

    event OnEnteringWarningRange(): void
    {
        DisplayMessage("*" + GetRandomMessage(warningMessages) + "*");
        lastMessageGameTime = theGame.GetGameTime();
    }

    event OnInit(): void
    {
        Init();
        InitMessages();
        InitValue();
        DisplayNotification("Initialized.");
    }

    event OnLeavingAlertRange(): void
    {}

    event OnLeavingMax(): void
    {}

    event OnLeavingMin(): void
    {}

    event OnLeavingSafeRange(): void
    {}

    event OnLeavingWarningRange(): void
    {}

    event OnMeditationEnd(_gameMinutesPassed: float): void
    {}

    event OnPeriodicDepleter(): void
    {
        var witcher : W3PlayerWitcher;
        var amount  : float;
        amount = GetConfigDepletionPerGameMinute();
        witcher = GetWitcherPlayer();
        if (witcher.GetIsWalking())   amount *= GetConfigDepletionMultiplierWhenWalking();
        if (witcher.GetIsRunning())   amount *= GetConfigDepletionMultiplierWhenRunning();
        if (witcher.GetIsSprinting()) amount *= GetConfigDepletionMultiplierWhenSprinting();
        if (witcher.IsSwimming())     amount *= GetConfigDepletionMultiplierWhenSwimming();
        UpdateValueBy(-amount, 'OnPeriodicDepleter');
    }

    event OnRecoverFromFainting(): void
    {
        if (GetValue() < GetAlertThreshold()) UpdateValueBy(GetAlertThreshold(), 'OnRecoverFromFainting');
    }

    event OnRoll(): void
    {
        UpdateValueBy(-GetConfigDepletionPerRoll(), 'OnRoll');
    }

    event OnStayingAlertRange(): void
    {
        if (!ShouldDisplayMessage()) return false;
        DisplayMessage("*" + GetRandomMessage(alertMessages) + "*");
        lastMessageGameTime = theGame.GetGameTime();
    }

    event OnStayingMax(): void
    {}

    event OnStayingMin(): void
    {
        var witcher: W3PlayerWitcher;
        var damageTaken: float;
        var nextHealth: float;
        witcher = GetWitcherPlayer();
        if (GetConfigDeathOnDepleted())
        {
            damageTaken = witcher.GetMaxHealth() / 60;
            nextHealth = witcher.GetHealth() - damageTaken;
            if (nextHealth < 0.f) nextHealth = 0.f;
            witcher.SetHealth(nextHealth);
            if (witcher.GetHealth() <= 0.f) witcher.Kill(GetClassName());
        }
        else
        {
            if (witcher.IsSwimming())   witcher.Kill(GetClassName());
            else                        witcher.ITOW_PushFaintState();
        }
    }

    event OnStayingSafeRange(): void
    {}

    event OnStayingWarningRange(): void
    {
        if (!ShouldDisplayMessage()) return false;
        DisplayMessage("*" + GetRandomMessage(warningMessages) + "*");
        lastMessageGameTime = theGame.GetGameTime();
    }

    event OnUpdate(previousValue: float): void
    {
        if (GetValue() == GetMax() && previousValue == GetMax())                        OnStayingMax();
        if (GetValue() == GetMax() && previousValue != GetMax())                        OnEnteringMax();
        if (GetValue() != GetMax() && previousValue == GetMax())                        OnLeavingMax();
        if (IsValueInSafeRange(GetValue()) && IsValueInSafeRange(previousValue))        OnStayingSafeRange();
        if (IsValueInSafeRange(GetValue()) && !IsValueInSafeRange(previousValue))       OnEnteringSafeRange();
        if (!IsValueInSafeRange(GetValue()) && IsValueInSafeRange(previousValue))       OnLeavingSafeRange();
        if (IsValueInWarningRange(GetValue()) && IsValueInWarningRange(previousValue))  OnStayingWarningRange();
        if (IsValueInWarningRange(GetValue()) && !IsValueInWarningRange(previousValue)) OnEnteringWarningRange();
        if (!IsValueInWarningRange(GetValue()) && IsValueInWarningRange(previousValue)) OnLeavingWarningRange();
        if (IsValueInAlertRange(GetValue()) && IsValueInAlertRange(previousValue))      OnStayingAlertRange();
        if (IsValueInAlertRange(GetValue()) && !IsValueInAlertRange(previousValue))     OnEnteringAlertRange();
        if (!IsValueInAlertRange(GetValue()) && IsValueInAlertRange(previousValue))     OnLeavingAlertRange();
        if (GetValue() == GetMin() && previousValue == GetMin())                        OnStayingMin();
        if (GetValue() == GetMin() && previousValue != GetMin())                        OnEnteringMin();
        if (GetValue() != GetMin() && previousValue == GetMin())                        OnLeavingMin();
    }

    protected function GetClassName(): CName
    {
        return CLASS_NAME;
    }

    protected function UpdateValueBy(amount: float, source: CName): void
    {
        var previousValue:      float;
        var nextPossibleValue:  float;
        if (!GetConfigEnabled()) return;
        previousValue = GetValue();
        nextPossibleValue = previousValue + amount;
        if (nextPossibleValue > GetMax())       SetValue(GetMax());
        else if (nextPossibleValue < GetMin())  SetValue(GetMin());
        else                                    SetValue(nextPossibleValue);
        DisplayNotification("Value changed by " + FloatToString(amount) + " from " + FloatToString(previousValue) + " to " + FloatToString(GetValue()) + ". (" + source + ")");
        OnUpdate(previousValue);
    }

    private function DisplayMessage(message: string): void
    {
        ITOW_displayer.DisplayMessage(message);
    }

    private function DisplayNotification(notification: string): void
    {
        if (!GetConfigDebugMode()) return;
        ITOW_displayer.DisplayNotification(GetClassName() + ": " + notification);
    }

    private function GetAlertThreshold(): int
    {
        return ALERT_THRESHOLD;
    }

    private function GetConfigDeathOnDepleted(): bool
    {
        return ITOW_config.Get(GetClassName(), GetConfigDeathOnDepletedKey());
    }

    private function GetConfigDeathOnDepletedKey(): CName
    {
        return CONFIG_DEATH_ON_DEPLETED_KEY;
    }

    private function GetConfigDebugMode(): bool
    {
        return ITOW_config.Get(GetClassName(), GetConfigDebugModeKey());
    }

    private function GetConfigDebugModeKey(): CName
    {
        return CONFIG_DEBUG_MODE_KEY;
    }

    private function GetConfigDepletionMultiplierWhenRunning(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionMultiplierWhenRunningKey()));
    }

    private function GetConfigDepletionMultiplierWhenRunningKey(): CName
    {
        return CONFIG_DEPLETION_MULTIPLIER_WHEN_RUNNING_KEY;
    }

    private function GetConfigDepletionMultiplierWhenSprinting(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionMultiplierWhenSprintingKey()));
    }

    private function GetConfigDepletionMultiplierWhenSprintingKey(): CName
    {
        return CONFIG_DEPLETION_MULTIPLIER_WHEN_SPRINTING_KEY;
    }

    private function GetConfigDepletionMultiplierWhenSwimming(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionMultiplierWhenSwimmingKey()));
    }

    private function GetConfigDepletionMultiplierWhenSwimmingKey(): CName
    {
        return CONFIG_DEPLETION_MULTIPLIER_WHEN_SWIMMING_KEY;
    }

    private function GetConfigDepletionMultiplierWhenWalking(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionMultiplierWhenWalkingKey()));
    }

    private function GetConfigDepletionMultiplierWhenWalkingKey(): CName
    {
        return CONFIG_DEPLETION_MULTIPLIER_WHEN_WALKING_KEY;
    }

    private function GetConfigDepletionPerAttack(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerAttackKey()));
    }

    private function GetConfigDepletionPerAttackKey(): CName
    {
        return CONFIG_DEPLETION_PER_ATTACK_KEY;
    }

    private function GetConfigDepletionPerConsumedAlcohol(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerConsumedAlcoholKey()));
    }

    private function GetConfigDepletionPerConsumedAlcoholKey(): CName
    {
        return CONFIG_DEPLETION_PER_CONSUMED_ALCOHOL_KEY;
    }

    private function GetConfigDepletionPerConsumedDrink(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerConsumedDrinkKey()));
    }

    private function GetConfigDepletionPerConsumedDrinkKey(): CName
    {
        return CONFIG_DEPLETION_PER_CONSUMED_DRINK_KEY;
    }

    private function GetConfigDepletionPerConsumedFood(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerConsumedFoodKey()));
    }

    private function GetConfigDepletionPerConsumedFoodKey(): CName
    {
        return CONFIG_DEPLETION_PER_CONSUMED_FOOD_KEY;
    }

    private function GetConfigDepletionPerConsumedPotion(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerConsumedPotionKey()));
    }

    private function GetConfigDepletionPerConsumedPotionKey(): CName
    {
        return CONFIG_DEPLETION_PER_CONSUMED_POTION_KEY;
    }

    private function GetConfigDepletionPerCounter(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerCounterKey()));
    }

    private function GetConfigDepletionPerCounterKey(): CName
    {
        return CONFIG_DEPLETION_PER_COUNTER_KEY;
    }

    private function GetConfigDepletionPerDodge(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerDodgeKey()));
    }

    private function GetConfigDepletionPerDodgeKey(): CName
    {
        return CONFIG_DEPLETION_PER_DODGE_KEY;
    }

    private function GetConfigDepletionPerJump(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerJumpKey()));
    }

    private function GetConfigDepletionPerJumpKey(): CName
    {
        return CONFIG_DEPLETION_PER_JUMP_KEY;
    }

    private function GetConfigDepletionPerParry(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerParryKey()));
    }

    private function GetConfigDepletionPerParryKey(): CName
    {
        return CONFIG_DEPLETION_PER_PARRY_KEY;
    }

    private function GetConfigDepletionPerGameMinute(): float
    {
        return GetConfigGameHourlyDepletion() / 60.f;
    }

    private function GetConfigDepletionPerRoll(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigDepletionPerRollKey()));
    }

    private function GetConfigDepletionPerRollKey(): CName
    {
        return CONFIG_DEPLETION_PER_ROLL_KEY;
    }

    private function GetConfigEnabled(): bool
    {
        return ITOW_config.Get(GetClassName(), GetConfigEnabledKey());
    }

    private function GetConfigEnabledKey(): CName
    {
        return CONFIG_ENABLED_KEY;
    }

    private function GetConfigGameHourlyDepletion(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigGameHourlyDepletionKey()));
    }

    private function GetConfigGameHourlyDepletionKey(): CName
    {
        return CONFIG_GAME_HOURLY_DEPLETION_KEY;
    }

    private function GetConfigMessageFrequencyInHour(): int
    {
        return StringToInt(ITOW_config.Get(GetClassName(), GetConfigMessageFrequencyInHourKey()));
    }

    private function GetConfigMessageFrequencyInHourKey(): CName
    {
        return CONFIG_MESSAGE_FREQUENCY_IN_HOUR_KEY;
    }

    private function GetConfigValue(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigValueKey()));
    }

    private function GetConfigValueKey(): CName
    {
        return CONFIG_VALUE_KEY;
    }

    private function GetMax(): float
    {
        return MAX;
    }

    private function GetMin(): float
    {
        return MIN;
    }

    private function GetRandomMessage(messages: array<string>): string
    {
        return messages[RandRange(messages.Size())];
    }

    private function GetValue(): float
    {
        return value;
    }

    private function GetWarningThreshold(): int
    {
        return WARNING_THRESHOLD;
    }

    private function Init(): void
    {
        ITOW_config     = new ITOW_Config in this;
        ITOW_displayer  = new ITOW_Displayer in this;
        ITOW_item       = new ITOW_Item in this;
        ITOW_config.OnInit();
        ITOW_displayer.OnInit();
        ITOW_item.OnInit();
        lastMessageGameTime = theGame.GetGameTime();
    }

    private function InitMessages(): void
    {
        SetMessagesAlert();
        SetMessagesDying();
        SetMessagesSafe();
        SetMessagesWarning();
    }

    private function InitValue(): void
    {
        if (GetConfigValue() == GetMin())                   SetValue(GetMax());
        else if (GetConfigValue() < GetAlertThreshold())    SetValue(GetAlertThreshold());
        else if (GetConfigValue() < GetWarningThreshold())  SetValue(GetWarningThreshold());
        else                                                SetValue(GetConfigValue());
    }

    private function IsValueInAlertRange(value: float): bool
    {
        return value > GetMin() && value <= GetAlertThreshold();
    }

    private function IsValueInSafeRange(value: float): bool
    {
        return value > GetWarningThreshold();
    }

    private function IsValueInWarningRange(value: float): bool
    {
        return value > GetAlertThreshold() && value <= GetWarningThreshold();
    }

    private function SetConfigValue(amount: float): void
    {
        ITOW_config.Set(GetClassName(), GetConfigValueKey(), FloatToString(amount));
    }

    private function SetMessagesAlert(): void
    {}

    private function SetMessagesDying(): void
    {}

    private function SetMessagesSafe(): void
    {}

    private function SetMessagesWarning(): void
    {}

    private function SetValue(amount: float): void
    {
        value = amount;
        SetConfigValue(amount);
    }

    private function ShouldDisplayMessage(): bool
    {
        var currentGameTime: GameTime;
        currentGameTime = theGame.GetGameTime();
        if (GameTimeHours(currentGameTime) < (GameTimeHours(lastMessageGameTime) + GetConfigMessageFrequencyInHour()) % 24) return false;
        return true;
    }
}

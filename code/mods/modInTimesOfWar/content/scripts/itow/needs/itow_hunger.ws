class ITOW_Hunger extends ITOW_Need
{
    default CLASS_NAME                                          = 'ITOW_Hunger';
    default CONFIG_DEATH_ON_DEPLETED_KEY                        = 'ITOW_HungerDeathWhenDepleted';
    default CONFIG_DEBUG_MODE_KEY                               = 'ITOW_HungerDebugEnabled';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_RUNNING_KEY        = 'ITOW_HungerDepletionMultiplierWhenRunning';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_SPRINTING_KEY      = 'ITOW_HungerDepletionMultiplierWhenSprinting';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_SWIMMING_KEY       = 'ITOW_HungerDepletionMultiplierWhenSwimming';
    default CONFIG_DEPLETION_MULTIPLIER_WHEN_WALKING_KEY        = 'ITOW_HungerDepletionMultiplierWhenWalking';
    default CONFIG_DEPLETION_PER_ATTACK_KEY                     = 'ITOW_HungerDepletionPerAttack';
    default CONFIG_DEPLETION_PER_CONSUMED_ALCOHOL_KEY           = 'ITOW_HungerDepletionPerConsumedAlcohol';
    default CONFIG_DEPLETION_PER_CONSUMED_DRINK_KEY             = 'ITOW_HungerDepletionPerConsumedDrink';
    default CONFIG_DEPLETION_PER_CONSUMED_POTION_KEY            = 'ITOW_HungerDepletionPerConsumedPotion';
    default CONFIG_DEPLETION_PER_COUNTER_KEY                    = 'ITOW_HungerDepletionPerCounter';
    default CONFIG_DEPLETION_PER_DODGE_KEY                      = 'ITOW_HungerDepletionPerDodge';
    default CONFIG_DEPLETION_PER_JUMP_KEY                       = 'ITOW_HungerDepletionPerJump';
    default CONFIG_DEPLETION_PER_PARRY_KEY                      = 'ITOW_HungerDepletionPerParry';
    default CONFIG_DEPLETION_PER_ROLL_KEY                       = 'ITOW_HungerDepletionPerRoll';
    default CONFIG_ENABLED_KEY                                  = 'ITOW_HungerEnabled';
    default CONFIG_GAME_HOURLY_DEPLETION_KEY                    = 'ITOW_HungerGameHourlyDepletion';
    default CONFIG_MESSAGE_FREQUENCY_IN_HOUR_KEY                = 'ITOW_HungerMessageFrequencyInHour';
    default CONFIG_VALUE_KEY                                    = 'ITOW_HungerValue';
    private const var CONFIG_RESTORATION_PER_CONSUMED_FOOD_KEY  : CName;
    default CONFIG_RESTORATION_PER_CONSUMED_FOOD_KEY            = 'ITOW_HungerRestorationPerConsumedFood';

    private function GetConfigDepletionPerConsumedFood(): float
    {
        return -GetConfigRestorationPerConsumedFood();
    }

    private function GetConfigRestorationPerConsumedFood(): float
    {
        return StringToFloat(ITOW_config.Get(GetClassName(), GetConfigRestorationPerConsumedFoodKey()));
    }

    private function GetConfigRestorationPerConsumedFoodKey(): CName
    {
        return CONFIG_RESTORATION_PER_CONSUMED_FOOD_KEY;
    }

    private function SetMessagesAlert(): void
    {
        var i: int;
        for (i = 0; i <= 3; i += 1) alertMessages.PushBack(GetLocStringByKeyExt("message_ITOW_HungerAlertMessage0" + i));
    }

    private function SetMessagesDying(): void
    {
        var i: int;
        for (i = 0; i <= 0; i += 1) dyingMessages.PushBack(GetLocStringByKeyExt("message_ITOW_HungerDyingMessage0" + i));
    }

    private function SetMessagesSafe(): void
    {
        var i: int;
        for (i = 0; i <= 4; i += 1) safeMessages.PushBack(GetLocStringByKeyExt("message_ITOW_HungerSafeMessage0" + i));
    }

    private function SetMessagesWarning(): void
    {
        var i: int;
        for (i = 0; i <= 4; i += 1) warningMessages.PushBack(GetLocStringByKeyExt("message_ITOW_HungerWarningMessage0" + i));
    }
}

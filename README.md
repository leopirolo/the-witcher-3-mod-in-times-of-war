# The Witcher 3 Mod - In Times of War

## About

Welcome to the world of "The Witcher 3: Wild Hunt," enhanced by the immersive and challenging mod "In Times of War." This mod introduces a realistic survival experience, adding three vital needs for Geralt: Hunger, Thirst, and Sleep. These needs will impact Geralt's well-being and gameplay, requiring you to manage his basic necessities to ensure his survival and success in his adventures.

## Features

- **Survival Mechanics**: Experience the hardships of Geralt's life by managing his hunger, thirst, and need for sleep. Ignoring these needs will have consequences, affecting his health and overall performance.
- **Consequences for Neglect**: If Geralt's needs are left unattended, he risks fainting, putting him at risk of losing valuable items while he sleeps.
- **Gameplay Enhancement**: Stay engaged in the immersive world of The Witcher 3 with added depth and challenges that mirror the demands of a real-life survivor.
- **Dynamic Solutions**: To satisfy his needs, you must find food, drink, and ways to rest. Meditate or consume potions to regain sleep, eat to fend off hunger, and drink to quench his thirst.

## Compatibility

- **Next Gen Version**: Please note that this mod is designed to work exclusively with the Next Gen version of "The Witcher 3: Wild Hunt."

## Installation

1. **Download and Install**: Get started by downloading and installing the mod using Vortex, a user-friendly mod manager.
2. **Configuration**: Open the game folder, go to `\bin\config\r4game\user_config_matrix\pc\` and add `modInTimesOfWar.xml;` at the end of both `dx11filelist.txt` and `dx12filelist.txt` files to ensure the mod is integrated seamlessly into your game.
3. **Customization**: Access the in-game options under `Settings/Mods/In Time of War` to tailor the mod's settings to your preference. Alternatively, use the "Default" setting for a balanced experience.

## Development and Contributions

- **Ongoing Development**: The mod is currently in development, and it appears to be stable.
- **Work in Progress**: The following enhancements are being worked on:
  - Interface improvements for clarity (Current notifications about Geralt's fatigue status need better representation. Bars of values can also be seen in the options, but they're not practical...)
  - Finding a way to close the menu when Geralt faints while the user is in the menu.
  - Implementing the exhaustion rate from parrying, countering, and jumping.
- **Photographers Welcome**: If you're a photographer or have a knack for capturing stunning visuals, we invite you to contribute captivating images for the mod's presentation.

## Community Involvement

- **Contributions Welcome**: Your contributions are valuable! If you're interested in translating the mod into other languages or assisting in its development, please visit our [GitLab repository](https://gitlab.com/leopirolo/the-witcher-3-mod-in-times-of-war/).
